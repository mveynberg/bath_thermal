#include <linux/device.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/of.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/thermal.h>

struct thermal_zone_device *thermal = NULL;

static int bath_get_temp(struct thermal_zone_device *thermal, int *temp)
{
  *temp = ((3220000000UL) / 13625);

  return 0;
}

static struct thermal_zone_device_ops ops =
  {
   .get_temp = bath_get_temp,
  };

static int __init bath_thermal_init(void)
{
  printk(KERN_INFO "Bath!!!\n");

  thermal = thermal_zone_device_register("bath_thermal", 0, 0,
                                         NULL, &ops, NULL, 0, 0);
  if (IS_ERR(thermal)) {
    printk(KERN_INFO "Device Driver Insert...Failed!!!\n");
    return PTR_ERR(thermal);
  }

  printk(KERN_INFO "Device Driver Insert...Done!!!\n");

  return 0;
}

void __exit bath_thermal_exit(void)
{
  thermal_zone_device_unregister(thermal);

  printk(KERN_INFO "Device Driver Insert...Done!!!\n");
}


module_init(bath_thermal_init);
module_exit(bath_thermal_exit);

MODULE_AUTHOR("Maksim Veynberg <mv@cj264.ru>");
MODULE_DESCRIPTION("bath thermal driver");
MODULE_LICENSE("GPL");
